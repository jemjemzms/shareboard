<div>
	<?php if(isset($_SESSION['is_logged_in'])) : ?>
	<a class="btn btn-success btn-share" href="<?php echo ROOT_PATH; ?>shares/add">Share Something</a>
	<?php
	endif;
	?>
	<?php foreach($viewmodel as $item) : ?>
		<div class="card card-inverse" style="background-color: #eaeaea; border-color: #eaeaea; margin:2px; padding:5px;">
		  <div class="card-block">
		    <h3><?php echo $item['title']; ?></h3>
		    <small><?php echo $item['create_date']; ?></small>
		    <hr />
		    <p><?php echo $item['body']; ?></p>
		    <br />
		    <a class="btn btn-info" href="<?php echo $item['link']; ?>" target="_blank">Go to website</a>
		  </div>
		</div>
	<?php endforeach; ?>
</div>