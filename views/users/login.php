<div class="card" style="width: 20rem;">
  <div class="card-body">
    <h4 class="card-title">Login</h4>
    <p class="card-text">
    		<form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
	    		<div class="form-group">
		    		<label>Email</label>
		    		<input type="text" name="email" class="form-control" />
	    		</div>
	    		<div class="form-group">
		    		<label>Password</label>
		    		<input type="password" name="password" class="form-control" />
	    		</div>
	    		<input class="btn btn-primary" name="submit" type="submit" value="Submit" />
    		</form>
    </p>
  </div>
</div>