<html>
<head>
	<title>Shareboard</title>
	<link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets/library/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php echo ROOT_URL; ?>assets/css/app.css" />
</head>
<body>
	<nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <a class="navbar-brand" href="<?php echo ROOT_URL; ?>">Shareboard</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>shares">Shares</a>
          </li>
        </ul>
        <?php
        if(isset($_SESSION['is_logged_in'])) : ?>
        <ul class="navbar-nav navbar-right">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>">Welcome <?php echo $_SESSION['user_data']['name']; ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>users/logout">Logout</a>
          </li>
        </ul>
        <?php else : ?>
        <ul class="navbar-nav navbar-right">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>users/login">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo ROOT_URL; ?>users/register">Register</a>
          </li>
        </ul>
        <?php
        endif;
        ?>
      </div>
    </nav>
    <?php Messages::display(); ?>
    <div class="container">

      <div class="row">

       <?php require($view); ?>
      </div>

    </div><!-- /.container -->
</body>
</html>